﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Iternity.ArchLint.MicrosoftCodeAnalysis
{
    /// <summary>
    /// <see cref="DependencyCollector"/> is a <see cref="CSharpSyntaxWalker"/>.
    /// It scans all "using" statements of a <see cref="Solution"/> and creates a set of <see cref="Dependency{Component}"/> instances between <see cref="Component"/>s.
    /// A <see cref="Component"/> is created from a namespace.
    /// </summary>
    public class DependencyCollector : CSharpSyntaxWalker
    {
        public readonly ISet<Dependency<Component>> Dependencies = new HashSet<Dependency<Component>>();
        public IComponentFactory _componentFactory { set; private get; }

        /// <summary>
        /// Creates a <see cref="DependencyCollector"/> with a given IComponentFactory
        /// </summary>
        /// <param name="componentFactory"></param>
        public DependencyCollector(IComponentFactory componentFactory)
        {
            _componentFactory = componentFactory;
        }

        /// <summary>
        /// Collects <see cref="Dependency{Component}"/> instances from this <see cref="Solution"/>.
        /// To create an instance of <see cref="Solution"/> you have to call something like
        /// <code>Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create().OpenSolutionAsync(pathToDotSlnFile).Result</code>.
        /// To make this work properly you also have to NuGet 
        /// <code>Microsoft.CodeAnalysis.CSharp.Workspaces</code> 
        /// and 
        /// <code>Microsoft.CodeAnalysis.Workspaces.Common</code>.
        /// </summary>
        /// <param name="solution"></param>
        /// <returns></returns>
        public ISet<Dependency<Component>> Collect(Solution solution)
        {
            foreach (var project in solution.Projects)
            {
                Collect(project);
            }
            return Dependencies;
        }

        /// <summary>
        /// Collects <see cref="Dependency{Component}"/> instances from this <see cref="Project"/>.
        /// To create an instance of <see cref="Project"/> you have to call something like
        /// <code>Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create().OpenProjectAsync(pathToDotCsprojFile).Result</code>.
        /// To make this work properly you also have to NuGet 
        /// <code>Microsoft.CodeAnalysis.CSharp.Workspaces</code> 
        /// and 
        /// <code>Microsoft.CodeAnalysis.Workspaces.Common</code>.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public ISet<Dependency<Component>> Collect(Project project)
        {
            foreach (var document in project.Documents)
            {
                collect(document);
            }
            return Dependencies;
        }

        private void collect(Document document)
        {
            Task<SyntaxNode> task = document.GetSyntaxRootAsync();
            task.ContinueWith(t =>
            {
                collect(t.Result);
            });
            task.Wait();
        }

        private void collect(SyntaxNode root)
        {
            Visit(root);
        }

        public override void VisitNamespaceDeclaration(NamespaceDeclarationSyntax ns)
        {
            // Add Dependency for every "using" declared inside the namespace.
            // Attention: This also collects unused "usings".
            foreach (var u in ns.Usings)
            {
                addDependency(ns, u);
            }

            // Add Dependency for every "using" declared on top of the compilation unit.
            // Attention: This also collects unused "usings".
            if (ns.Parent.IsKind(SyntaxKind.CompilationUnit))
            {
                CompilationUnitSyntax cu = ((CompilationUnitSyntax)ns.Parent);
                foreach (var u in cu.Usings)
                {
                    addDependency(ns, u);
                }
            }
        }

        private void addDependency(NamespaceDeclarationSyntax from, UsingDirectiveSyntax to)
        {
            Component fromComponent = _componentFactory.CreateComponent(from.Name.ToString());
            Component toComponent = _componentFactory.CreateComponent(to.Name.ToString());
            Dependency<Component> dep = new Dependency<Component>(fromComponent, toComponent);
            Dependencies.Add(dep);
        }

    }

}
