﻿namespace Iternity.ArchLint
{
    /// <summary>
    /// This architecture consists of two concentric layers inspired by Bob Martins 
    /// Clean Architecture (https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html).
    /// 
    /// The inner layer is called "CORE". It represents the core domain logic and domain model.
    /// 
    /// The outer layer is called "IO" (Input/Output). It contains components that connect to an
    /// external process, technology or user interaction.
    /// 
    /// Besides the two (IO and CORE) there are two more kinds of components in this architectural style.
    /// - MAIN - A component that assembles all components and bootstraps the app.
    /// - UNKNOWN - Anything that is not of interest.
    /// 
    /// The dependency rules of this architectural style are:
    /// - Every component may depend on CORE components.
    /// - An IO component may not depend on other IO components.
    /// - A CORE component may not depend on any IO component.
    /// - Any component other than CORE and IO does not have any restrictions.
    /// 
    /// To identify the different kinds of components (MAIN, IO, CORE, UNKNOWN) this architectural style uses 
    /// following naming convention with `.` (dot) seperated component names:
    /// - Every component of kind MAIN, IO and CORE has the same, user defined prefix.
    /// - Every component of kind MAIN has a name with one segment(no dot).
    /// - Every component of kind CORE has a name with two segments(one dot).
    /// - Every component of kind IO has a name with three segments(two dots).
    /// - Every component that does not fit into one of the above definitions is of kind UNKNOWN.
    /// </summary>
    public class CleanArchLight : IArchStyle
    {
        public static class ComponentKinds
        {
            public static ComponentKind MAIN = new ComponentKind("MAIN");   // A MAIN component assembles and starts the application.
            public static ComponentKind CORE = new ComponentKind("CORE");   // A CORE component represents a part of the core domain logic (perhaps a single subdomain).
            public static ComponentKind IO = new ComponentKind("IO");       // An IO (Input/Output) component encapsulates a connection to an external processes or technology.
            public static ComponentKind UNKNOWN = ComponentKind.UNKNOWN;    // Every other component is an UNKNOWN component, thus out of scope.
        }

        private readonly string _domain;

        /// <summary>
        /// MAIN, CORE and IO Components must be prefixed with the given "domain".
        /// Example: domain = "Petshop"
        /// - Component with name "Petshop" is of kind MAIN
        /// - Component with name "Petshop.Order" is of kind CORE
        /// - Component with name "Petshop.Order.Web" is of kind IO
        /// Everything that does not start with "Petshop" is an UNKNOWN Component. 
        /// </summary>
        /// <param name="domain"></param>
        public CleanArchLight(string domain)
        {
            _domain = domain;
        }

        public Component CreateComponent(string name)
        {
            return new Component(name, createComponentKindFor(name));
        }

        private ComponentKind createComponentKindFor(string name)
        {
            if (!name.StartsWith(_domain))
            {
                return ComponentKinds.UNKNOWN;
            }

            var segments = name.Split('.');
            switch (segments.Length)
            {
                case 1: return ComponentKinds.MAIN;
                case 2: return ComponentKinds.CORE;
                case 3: return ComponentKinds.IO;
                default: return ComponentKinds.UNKNOWN;
            }
        }

        public bool IsAllowed<T>(Dependency<T> dependency) where T : IKinded
        {
            var from = dependency.From;
            var to = dependency.To;
            return from.IsKind(ComponentKinds.MAIN)     // A MAIN component may depend an all other component types.
                || to.IsKind(ComponentKinds.CORE)       // Every component type may depend on CORE components.
                || to.IsKind(ComponentKinds.UNKNOWN);   // Every component type may depend on UNKNOWN components.
        }
    }
}