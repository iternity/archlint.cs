﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Iternity.ArchLint.PlantUML
{
    /// <summary>
    /// Creates plantuml component diagramm for a given set of dependencies or layers.
    /// Invalid dependencies are labeled with "INVALID".
    /// </summary>
    public class PlantUMLWriter
    {
        private IDependencyRule _dependencyRule;
        private TextWriter _writer;
        private Boolean _onlyInvalidDependencies;
        private Boolean _withLayerDependencies;

        /// <summary>
        /// Convenient factory method to support fluent API style.
        /// Creates a <see cref="PlantUMLWriter"/> with default settings:
        /// - Use Console.Out as writer.
        /// - Print all dependencies between components (valid and invalid)
        /// - Print all layer depedencies.
        /// </summary>
        /// <param name="dependencyRule"></param>
        /// <returns></returns>
        public static PlantUMLWriter WithRule(IDependencyRule dependencyRule)
        {
            return new PlantUMLWriter(dependencyRule);
        }

        /// <summary>
        /// Creates a <see cref="PlantUMLWriter"/> with default settings:
        /// - Use Console.Out as writer.
        /// - Print all dependencies between components (valid and invalid)
        /// - Print all layer depedencies.
        /// </summary>
        /// <param name="dependencyRule"></param>
        public PlantUMLWriter(IDependencyRule dependencyRule)
        {
            _dependencyRule = dependencyRule;
            _writer = Console.Out;
            _onlyInvalidDependencies = false;
            _withLayerDependencies = true;
        }

        /// <summary>
        /// Call if you want to print only invalid dependencies.
        /// </summary>
        public PlantUMLWriter OnlyInvalidDependencies()
        {
            _onlyInvalidDependencies = true;
            return this;
        }

        /// <summary>
        /// Call if you want to exclude layer dependencies.
        /// </summary>
        public PlantUMLWriter ExcludeLayerDependencies()
        {
            _withLayerDependencies = false;
            return this;
        }

        /// <summary>
        /// Call if you want to override the default write (<see cref="Console.Out"/>)
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public PlantUMLWriter WithWriter(TextWriter writer)
        {
            _writer = writer;
            return this;
        }
        
        /// <summary>
        /// Print the <see cref="Dependency{Component}"/>s to the given <see cref="_writer"/>
        /// </summary>
        /// <param name="componentDependencies"></param>
        public void Write(ISet<Dependency<Component>> componentDependencies)
        {
            Write(new Layers(componentDependencies));
        }

        /// <summary>
        /// Print the <see cref="Layer"/>s to the given <see cref="_writer"/>
        /// </summary>
        /// <param name="layers"></param>
        public void Write(Layers layers)
        {
            _writer.WriteLine("@startuml");
            _writer.WriteLine("'Paste this in http://www.plantuml.com/plantuml to visualize an UML component diagram'");
            _writer.WriteLine("skinparam componentStyle uml2");
            _writer.WriteLine("skinparam rectangle {");
            _writer.WriteLine("    roundCorner << Layer >> 25");
            _writer.WriteLine("}");
            writeLayerDefinitions(layers);
            writeComponentDependencies(layers);
            writeLayerDependencies(layers);
            _writer.WriteLine("@enduml");
        }
        
        private void writeLayerDefinitions(Layers layers)
        {
            foreach (var layer in layers)
            {
                write(layer);
            }
        }

        private void writeComponentDependencies(Layers layers)
        {
            foreach (var dep in layers.ComponentDependencies)
            {
                if (!_onlyInvalidDependencies || !_dependencyRule.IsAllowed(dep))
                {
                    write(dep);
                }
            }
        }

        private void writeLayerDependencies(Layers layers)
        {
            if (!_withLayerDependencies) return;
            foreach (var dep in layers.LayerDependencies)
            {
                write(dep);
            }
        }

        private void write(Layer layer)
        {
            _writer.WriteLine("rectangle " + layer.Kind + " <<Layer>> {");
            foreach (var c in layer.Components)
            {
                _writer.Write("\t");
                write(c);
            }
            _writer.WriteLine("}");
        }

        private void write(Component component)
        {
            _writer.WriteLine("[" + component.Name + "] <<" + component.GetKind() + ">>");
        }

        private void write(Dependency<Component> dependency)
        {
            string label = "";
            if (!_dependencyRule.IsAllowed(dependency))
            {
                label = " : INVALID";
            }
            _writer.WriteLine("[" + dependency.From.Name + "] ..> [" + dependency.To.Name + "]" + label);
        }

        private void write(Dependency<Layer> dependency)
        {
            string label = "";
            if (!_dependencyRule.IsAllowed(dependency))
            {
                label = " : INVALID";
            }
            _writer.WriteLine(dependency.From.Kind + " ..> " + dependency.To.Kind + label);
        }
    }

}
