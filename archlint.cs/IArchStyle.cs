﻿namespace Iternity.ArchLint
{
    public interface IArchStyle : IComponentFactory, IDependencyRule
    {
    }

    public interface IDependencyRule
    {
        bool IsAllowed<T>(Dependency<T> dependency) where T : IKinded;
    }

    public interface IComponentFactory
    {
        Component CreateComponent(string name);
    }

    internal class NoDependencyRestrictions : IDependencyRule
    {
        public bool IsAllowed<T>(Dependency<T> dependency) where T : IKinded
        {
            return true;
        }
    }
}