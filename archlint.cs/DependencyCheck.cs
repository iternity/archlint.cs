﻿using Microsoft.CodeAnalysis;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Iternity.ArchLint
{

    /// <summary>
    /// <see cref="DependencyCheck"/> performs an analysis of all given dependencies between components and layers.
    /// </summary>
    public class DependencyCheck
    {
        /// <summary>
        /// Use a ComponentFilter to filter the components before analysis.
        /// This can reduce complexity of the analysis result.
        /// </summary>
        /// <param name="componentName"></param>
        /// <returns>true if the component should be used. False otherwise.</returns>
        public delegate bool ComponentFilter(String componentName);

        /// <summary>
        /// Factory method. Use this to create an instance.
        /// This is the start point of the fluent API of <see cref="DependencyCheck"/>
        /// </summary>
        /// <param name="dependencies"></param>
        /// <returns>an instance of <see cref="DependencyCheck"/> as start point of the fluent API</returns>
        public static DependencyCheck WithDependencies(ISet<Dependency<Component>> dependencies)
        {
            return new DependencyCheck(dependencies);
        }

        private ISet<Dependency<Component>> _dependencies;
        private IDependencyRule _dependencyRule = new NoDependencyRestrictions();

        public DependencyCheck(ISet<Dependency<Component>> dependencies)
        {
            if (dependencies == null)
            {
                dependencies = new HashSet<Dependency<Component>>();
            }
            _dependencies = dependencies;
        }

        /// <summary>
        /// Optional filter for dependencies.
        /// </summary>
        /// <param name="componentFilter"></param>
        /// <returns>this so that you can continue using the fluent API</returns>
        public DependencyCheck FilteredBy(ComponentFilter componentFilter)
        {
            var filteredDependencies = new HashSet<Dependency<Component>>();
            foreach (var dep in _dependencies)
            {
                if (componentFilter(dep.To.Name) && componentFilter(dep.From.Name))
                {
                    filteredDependencies.Add(dep);
                }
            }
            _dependencies = filteredDependencies;
            return this;
        }

        /// <summary>
        /// Optional <see cref="IDependencyRule"/>. 
        /// By default a <see cref="NoDependencyRestrictions"/> rule is used.
        /// So you probably want to call this before computing the <see cref="Result"/>.
        /// </summary>
        /// <param name="dependencyRule"></param>
        /// <returns>this so that you can continue using the fluent API</returns>
        public DependencyCheck WithRule(IDependencyRule dependencyRule)
        {
            if (dependencyRule != null)
            {
                _dependencyRule = dependencyRule;
            }
            return this;
        }

        /// <summary>
        /// Computes the <see cref="DependencyCheckResult"/> applying
        /// - the given <see cref="IDependencyRule"/>
        /// - on the given <see cref="Dependency{Component}"/> list
        /// - that is optinally filtered by <see cref="ComponentFilter"/>.
        /// </summary>
        /// <returns>the <see cref="DependencyCheckResult"/></returns>
        public DependencyCheckResult Result()
        {
            if (_dependencyRule == null)
            {
                _dependencyRule = new NoDependencyRestrictions();
            }
            return new DependencyCheckResult()
            {
                AllDependenciesAllowed = _dependencies.All(dep => _dependencyRule.IsAllowed(dep)),
                DependencyRule = _dependencyRule,
                Dependencies = _dependencies
            };
        }
    }

    /// <summary>
    /// Simple data object holding the result of the <see cref="DependencyCheck"/> and the used <see cref="IDependencyRule"/>.
    /// The state is immutable for non-internal classes.
    /// </summary>
    public class DependencyCheckResult
    {
        public bool AllDependenciesAllowed { get; internal set; }

        // TODO Instead of "DependencyRule" and "Dependencies" return a set of "CheckedDependencies"
        public IDependencyRule DependencyRule { get; internal set; }
        public ISet<Dependency<Component>> Dependencies { get; internal set; }
    }

    /// <summary>
    /// Provides some convenience methods to write a <see cref="DependencyCheckResult"/>
    /// </summary>
    public class DependencyCheckResultWriter
    {
        private DependencyCheckResult _result;
        private TextWriter _writer;
        private bool _onlyInvalidDependencies;

        /// <summary>
        /// Convenient factory method to support fluent API style.
        /// Creates a <see cref="DependencyCheckResultWriter"/> with default settings
        /// - use <see cref="Console.out"/> as <see cref="TextWriter"/>
        /// - write all dependencies including the invalid ones.
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public static DependencyCheckResultWriter WithResult(DependencyCheckResult result)
        {
            return new DependencyCheckResultWriter(result);
        }

        /// <summary>
        /// Creates a <see cref="DependencyCheckResultWriter"/> with default settings
        /// - use <see cref="Console.out"/> as <see cref="TextWriter"/>
        /// - write all dependencies including the invalid ones.
        /// </summary>
        /// <param name="result"></param>
        public DependencyCheckResultWriter(DependencyCheckResult result)
        {
            _result = result;
            _writer = Console.Out;
            _onlyInvalidDependencies = false;
        }

        /// <summary>
        /// Call this if you want to override the default <see cref="TextWriter"/> (<see cref="Console.out"/>)
        /// </summary>
        /// <param name="writer"></param>
        /// <returns></returns>
        public DependencyCheckResultWriter WithWriter(TextWriter writer)
        {
            _writer = writer;
            return this;
        }

        /// <summary>
        /// Call this if you want to print only invalid dependencies
        /// </summary>
        /// <returns></returns>
        public DependencyCheckResultWriter OnlyInvalidDependencies()
        {
            _onlyInvalidDependencies = true;
            return this;
        }

        /// <summary>
        /// If everything is configured by using the fluent API methods call this to write the result.
        /// </summary>
        public void Write()
        {
            if(_onlyInvalidDependencies) writeOnlyInvalid();
            else writeAll();
        }

        private void writeAll()
        {
            foreach (var dep in _result.Dependencies)
            {
                write(dep);
            }
        }

        private void writeOnlyInvalid()
        {
            foreach (var dep in _result.Dependencies)
            {
                if (!_result.DependencyRule.IsAllowed(dep))
                {
                    write(dep);
                }
            }
        }

        private void write(Dependency<Component> dep)
        {
            _writer.WriteLine(dep.ToString());
        }
    }
 
}
