﻿using System;

namespace Iternity.ArchLint
{
    /// <summary>
    /// A Component is a unit of a software architecture with a special concern.
    /// It is identified by a name and a <see cref="ComponentKind"/>.
    /// </summary>
    public class Component : IKinded
    {
        public readonly String Name;
        public readonly ComponentKind Kind;

        public Component(String name)
        {
            Name = name;
            Kind = ComponentKind.UNKNOWN;
        }

        public Component(String name, ComponentKind kind)
        {
            Name = name;
            Kind = kind;
        }

        public ComponentKind GetKind()
        {
            return Kind;
        }

        public Boolean IsKind(ComponentKind k)
        {
            return GetKind().Equals(k);
        }

        public override bool Equals(object obj)
        {
            return ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return "Component [" + Name + "] of kind <<" + GetKind() + ">>";
        }

    }

}
