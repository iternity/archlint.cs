﻿using System.Collections.Generic;

namespace Iternity.ArchLint
{
    public class ComponentKind
    {
        public static ComponentKind UNKNOWN = new ComponentKind("UNKNOWN");

        public string Name { get; private set; }

        public ComponentKind(string name)
        {
            Name = name;
        }

        public override bool Equals(object obj)
        {
            var kind = obj as ComponentKind;
            return kind != null &&
                   Name == kind.Name;
        }

        public override int GetHashCode()
        {
            return -1125283371 + EqualityComparer<string>.Default.GetHashCode(Name);
        }

        public override string ToString()
        {
            return Name;
        }
    }

}
