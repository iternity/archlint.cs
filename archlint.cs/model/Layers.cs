﻿using System.Collections.Generic;

namespace Iternity.ArchLint
{
    /// <summary>
    /// A set of Layers where each Layer holds a bunch of components of a specific kind.
    /// </summary>
    public class Layers : HashSet<Layer>
    {
        public readonly ISet<Component> Components = new HashSet<Component>();
        public readonly ISet<Dependency<Component>> ComponentDependencies = new HashSet<Dependency<Component>>();
        public readonly ISet<Dependency<Layer>> LayerDependencies = new HashSet<Dependency<Layer>>();

        public Layers(ISet<Dependency<Component>> componentDependencies)
        {
            if (componentDependencies == null) return;
            foreach (var dependency in componentDependencies)
            {
                Add(dependency);
                Add(dependency);
            }
        }

        /// <summary>
        /// The Dependency and both of its Components are added.
        /// </summary>
        /// <param name="dependency"></param>
        public void Add(Dependency<Component> dependency)
        {
            Layer fromLayer = Add(dependency.From);
            Layer toLayer = Add(dependency.To);

            ComponentDependencies.Add(dependency);
            LayerDependencies.Add(new Dependency<Layer>(fromLayer, toLayer));
        }

        /// <summary>
        /// The component is added to the Layer of the 
        /// </summary>
        /// <param name="component"></param>
        /// <returns>The layer that this component belongs to</returns>
        public Layer Add(Component component)
        {
            var layer = findLayerFor(component.GetKind());
            layer.Components.Add(component);
            Components.Add(component);
            return layer;
        }

        private Layer findLayerFor(ComponentKind kind)
        {
            foreach (var layer in this)
            {
                if (kind.Equals(layer.Kind))
                {
                    return layer;
                }
            }
            Layer newLayer = new Layer(kind);
            Add(newLayer);
            return newLayer;
        }

    }

}
