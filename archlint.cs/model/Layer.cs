﻿using System.Collections.Generic;

namespace Iternity.ArchLint
{
    /// <summary>
    /// A Layer is a set of components of a specific kind.
    /// </summary>
    public class Layer : IKinded
    {
        internal readonly ComponentKind Kind;
        internal readonly ISet<Component> Components = new HashSet<Component>();

        public Layer(ComponentKind kind)
        {
            Kind = kind;
        }

        public ComponentKind GetKind()
        {
            return Kind;
        }

        public bool IsKind(ComponentKind kind)
        {
            return Kind.Equals(kind);
        }

        public override bool Equals(object obj)
        {
            return Kind.Equals(((Layer)obj).GetType());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return "Layer of kind <<" + Kind + ">>";
        }
    }

}
