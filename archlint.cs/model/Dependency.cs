﻿using System;

namespace Iternity.ArchLint
{
    /// <summary>
    /// A Dependency between two Dependees (<see cref="Dependee"/>.
    /// The Dependency always have a unique direction.
    /// </summary>
    public class Dependency<T> where T : IKinded
    {
        public readonly T From;
        public readonly T To;

        /// <summary>
        /// Use NewDependency.From(...).To(...)
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        internal Dependency(T from, T to)
        {
            From = from;
            To = to;
        }

        public override bool Equals(object obj)
        {
            return ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return From.ToString() + " depends on " + To.ToString();
        }

    }

    /// <summary>
    /// Dependency Builder with fluent API (for better readability)
    /// </summary>
    public class NewDependency
    {
        public static NewDependency From(Component from)
        {
            return new NewDependency(from);
        }

        private Component _from;

        private NewDependency(Component from)
        {
            _from = from;
        }

        public Dependency<Component> To(Component to)
        {
            return new Dependency<Component>(_from, to);
        }

    }

}
