﻿using System;

namespace Iternity.ArchLint
{
    public interface IKinded
    {
        Boolean IsKind(ComponentKind kind);
        ComponentKind GetKind();
    }

}
