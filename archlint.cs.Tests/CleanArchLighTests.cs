﻿using Iternity.ArchLint;
using NUnit.Framework;

namespace archlint.cs.Tests
{
    [TestFixture]
    public class CleanArchLighTests
    {
        private IArchStyle _archStyle = new CleanArchLight("Petshop");

        [Test]
        public void DomainComponentsThatEqualsTheDomainNameAreOfKindMAIN()
        {
            Assert.IsTrue(anyMainComponent().IsKind(CleanArchLight.ComponentKinds.MAIN));
        }

        private Component anyMainComponent()
        {
            return _archStyle.CreateComponent("Petshop");
        }

        [Test]
        public void DomainComponentsWithTwoSegmentsAreOfKindCORE()
        {
            Assert.IsTrue(anyCoreComponent().IsKind(CleanArchLight.ComponentKinds.CORE));
        }

       
        [Test]
        public void DomainComponentsWithThreeSegmentsAreOfKindIO()
        {
            Assert.IsTrue(anyIoComponent().IsKind(CleanArchLight.ComponentKinds.IO));
        }

        [Test]
        public void DomainComponentsWithMoreThanThreeSegmentsAreOfKindUNKNOWN()
        {
            Assert.IsTrue(anotherUnknownComponent().IsKind(CleanArchLight.ComponentKinds.UNKNOWN));
        }

        [Test]
        public void ComponentsOutsideTheDomainAreOfKindUNKNOWN()
        {
            Assert.IsTrue(anyUnknownComponent().IsKind(CleanArchLight.ComponentKinds.UNKNOWN));
        }

        [Test]
        public void DependencyFromUnknownToEverywhereIsAllowed()
        {
            _archStyle.IsAllowed(NewDependency.From(anyUnknownComponent()).To(anyMainComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyUnknownComponent()).To(anyCoreComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyUnknownComponent()).To(anyIoComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyUnknownComponent()).To(anotherUnknownComponent()));
        }

        [Test]
        public void DependencyFromMainToEverywhereIsAllowed()
        {
            _archStyle.IsAllowed(NewDependency.From(anyMainComponent()).To(anyCoreComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyMainComponent()).To(anyIoComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyMainComponent()).To(anyUnknownComponent()));
        }

        [Test]
        public void DependencyToCoreFromEverywhereIsAllowed()
        {
            _archStyle.IsAllowed(NewDependency.From(anotherCoreComponent()).To(anyCoreComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyIoComponent()).To(anyCoreComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyMainComponent()).To(anyCoreComponent()));
            _archStyle.IsAllowed(NewDependency.From(anyUnknownComponent()).To(anyCoreComponent()));
        }

        [Test]
        public void DependencyFromCoreToIoIsNotAllowed()
        {
            _archStyle.IsAllowed(NewDependency.From(anyCoreComponent()).To(anotherIoComponent()));
        }

        [Test]
        public void DependencyFromIoToIoIsNotAllowed()
        {
            _archStyle.IsAllowed(NewDependency.From(anyIoComponent()).To(anotherIoComponent()));
        }

        private Component anyCoreComponent()
        {
            return _archStyle.CreateComponent("Petshop.Order");
        }

        private Component anotherCoreComponent()
        {
            return _archStyle.CreateComponent("Petshop.Shipping");
        }

        private Component anyIoComponent()
        {
            return _archStyle.CreateComponent("Petshop.Order.Web");
        }

        private Component anotherIoComponent()
        {
            return _archStyle.CreateComponent("Petshop.Order.MySQL");
        }

        private Component anyUnknownComponent()
        {
            return _archStyle.CreateComponent("System");
        }

        private Component anotherUnknownComponent()
        {
            return _archStyle.CreateComponent("Petshop.Order.Web.Foo");
        }

    }

}
