﻿using Iternity.ArchLint;
using Iternity.ArchLint.MicrosoftCodeAnalysis;
using Iternity.ArchLint.PlantUML;
using NUnit.Framework;
using Microsoft.CodeAnalysis;

namespace archlint.cs.Tests
{
    [TestFixture]
    public class End2EndTests
    {

        //[Test]
        // TODO ReadmeExample test needs a Solution mock or a test solution...
        public void ReadmeExample()
        {
            // 1. Choose your architectural style
            var archStyle = new CleanArchLight("Petshop");

            // 2. Identify and parse all components and dependencies in your solution (according to the given [definition](#Defintion: Components and Dependencies)).
            var petshopSolution = Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create().OpenSolutionAsync("path/To/Petshop/Solution.sln").Result;
            var dependencies = new DependencyCollector(archStyle).Collect(petshopSolution);

            // 3. Check for invalid component dependencies (according to your [architectural style](#Architectural styles)).
            var checkedDependencies = DependencyCheck
                    .WithDependencies(dependencies)
                    .WithRule(archStyle)
                    .Result();

            // 4. (Optional) Use a StringWriter to create a message for the upcoming Assert statement, where only the invalid dependencies are printed.
            var msgWriter = new System.IO.StringWriter();
            DependencyCheckResultWriter
                .WithResult(checkedDependencies)
                .WithWriter(msgWriter)
                .OnlyInvalidDependencies()
                .Write();

            // 5. Assert that all dependencies are allowed
            Assert.That(checkedDependencies.AllDependenciesAllowed, Is.True, msgWriter.ToString());

            // 6. (Optional) Create a low detailed PlantUML component diagram to visualize only invalid dependencies.  
            PlantUMLWriter
                .WithRule(archStyle)
                .OnlyInvalidDependencies()
                .ExcludeLayerDependencies()
                .Write(dependencies);

            // 7. (Optional) Create a fully blown PlantUML component diagram to visualize all components and dependencies.  
            PlantUMLWriter
                .WithRule(archStyle)
                .Write(dependencies);
        }

        private Solution solutionMock()
        {
            return null;
        }

    }

}
