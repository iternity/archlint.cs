﻿using Iternity.ArchLint;
using NUnit.Framework;
using System.Collections.Generic;
using System;

namespace archlint.cs.Tests
{
    class NoDependenciesAllowed : IDependencyRule
    {
        public bool IsAllowed<T>(Dependency<T> dependency) where T : IKinded
        {
            return false;
        }
    }

    class AllDependenciesAllowed : IDependencyRule
    {
        public bool IsAllowed<T>(Dependency<T> dependency) where T : IKinded
        {
            return true;
        }
    }

    [TestFixture]
    class DependencyCheckerTests
    {
        private bool noFilter(String componentName)
        {
            return true;
        }

        [Test]
        public void GivenAnEmptySetOfDependenciesWhenNoDependenciesAreAllowedTheResultIsTrue()
        {
            var dependencies = new HashSet<Dependency<Component>>();
            var result = DependencyCheck
                .WithDependencies(dependencies)
                .FilteredBy(noFilter)
                .WithRule(new NoDependenciesAllowed())
                .Result();

            Assert.That(result.AllDependenciesAllowed, Is.True);
        }

        [Test]
        public void GivenAnEmptySetOfDependenciesWhenAllDependenciesAreAllowedTheResultIsTrue()
        {
            var dependencies = new HashSet<Dependency<Component>>();
            var result = DependencyCheck
                .WithDependencies(dependencies)
                .FilteredBy(noFilter)
                .WithRule(new AllDependenciesAllowed())
                .Result();

            Assert.That(result.AllDependenciesAllowed, Is.True);
        }

        [Test]
        public void GivenASingleDependencyWhenNoDependenciesAreAllowedTheResultIsFalse()
        {
            var dependencies = new HashSet<Dependency<Component>>();
            dependencies.Add(NewDependency.From(new Component("A")).To(new Component("B")));
            var result = DependencyCheck
                .WithDependencies(dependencies)
                .FilteredBy(noFilter)
                .WithRule(new NoDependenciesAllowed())
                .Result();

            Assert.That(result.AllDependenciesAllowed, Is.False);
        }

        [Test]
        public void GivenADependencyBetweenComponentsOfKindUnknownWhenUsingCleanArchLightRulesTheResultIsTrue()
        {
            var dependencies = new HashSet<Dependency<Component>>();
            dependencies.Add(NewDependency.From(new Component("A")).To(new Component("B")));
            var result = DependencyCheck
                .WithDependencies(dependencies)
                .FilteredBy(noFilter)
                .WithRule(new CleanArchLight("Test"))
                .Result();

            Assert.That(result.AllDependenciesAllowed, Is.True);
        }

        [Test]
        public void GivenAllowedAndUnallowedDependenciesTheResultIsFalse()
        {
            Dependency<Component> unallowedDependency = NewDependency
                .From(new Component("IO-1", CleanArchLight.ComponentKinds.IO))
                .To(new Component("IO-2", CleanArchLight.ComponentKinds.IO));
            Dependency<Component> allowedDependency = NewDependency
                .From(new Component("CORE-1", CleanArchLight.ComponentKinds.CORE))
                .To(new Component("CORE-2", CleanArchLight.ComponentKinds.CORE));

            var dependencies = new HashSet<Dependency<Component>>();
            dependencies.Add(unallowedDependency);
            dependencies.Add(allowedDependency);

            var result = DependencyCheck
                .WithDependencies(dependencies)
                .FilteredBy(noFilter)
                .WithRule(new CleanArchLight("Test"))
                .Result();

            Assert.That(result.AllDependenciesAllowed, Is.False);
        }

        private bool onlyCore(String componentName)
        {
            return componentName.Equals(CleanArchLight.ComponentKinds.CORE.Name);
        }

        [Test]
        public void GiveAllowedAndUnallowedDependenciesWhenFilteringTheUnallowedOnesTheResultIsTrue()
        {
            Dependency<Component> unallowedDependency = NewDependency
                .From(new Component("IO-1", CleanArchLight.ComponentKinds.IO))
                .To(new Component("IO-2", CleanArchLight.ComponentKinds.IO));
            Dependency<Component> allowedDependency = NewDependency
                .From(new Component("CORE-1", CleanArchLight.ComponentKinds.CORE))
                .To(new Component("CORE-2", CleanArchLight.ComponentKinds.CORE));

            var dependencies = new HashSet<Dependency<Component>>();
            dependencies.Add(unallowedDependency);
            dependencies.Add(allowedDependency);

            var result = DependencyCheck
                .WithDependencies(dependencies)
                .FilteredBy(onlyCore)
                .WithRule(new CleanArchLight("Test"))
                .Result();

            Assert.That(result.AllDependenciesAllowed, Is.True);
        }
    }

}
