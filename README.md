The archlint project provides static code analysis (SCA) to verify certain architectural conditions
in your Visual Studio .NET solution. 
Especially component structures, layers and dependency rules between them.

When used continuously (e.g. as architectural tests inside your CI) archlint can help to prevent 
so called "architectural erosion" in your project, which is the gap between the planned and 
the actual architecture of your software system.

## How can I use it?
See how simple it is to automatically verify your architecture with a single Assertion as part of a unit test.

```c#
// 1. Choose your architectural style
var archStyle = new CleanArchLight("Petshop");

// 2. Identify and parse all components and dependencies in your solution (according to the given [definition](#Defintion: Components and Dependencies)).
var petshopSolution = Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create().OpenSolutionAsync("path/To/Petshop/Solution.sln").Result;
var dependencies = new DependencyCollector(archStyle).Collect(petshopSolution);

// 3. Check for invalid component dependencies (according to your [architectural style](#Architectural styles)).
var checkedDependencies = DependencyCheck
        .WithDependencies(dependencies)
        .WithRule(archStyle)
        .Result();

// 4. Assert that all dependencies are allowed
Assert.IsTrue(checkedDependencies.AllDependenciesAllowed, "There are invalid dependencies in your code!");

```

Here's another example with some more usefull features using pretty printing and the [iTernity.plantuml](https://www.nuget.org/packages/iTernity.plantuml/) package.
```c#
// 1. Choose your architectural style
var archStyle = new CleanArchLight("Petshop");

// 2. Identify and parse all components and dependencies in your solution (according to the given [definition](#Defintion: Components and Dependencies)).
var petshopSolution = Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create().OpenSolutionAsync("path/To/Petshop/Solution.sln").Result;
var dependencies = new DependencyCollector(archStyle).Collect(petshopSolution);

// 3. Check for invalid component dependencies (according to your [architectural style](#Architectural styles)).
var checkedDependencies = DependencyCheck
        .WithDependencies(dependencies)
        .WithRule(archStyle)
        .Result();

// 4. Use a StringWriter to create a message for the upcoming Assert statement, where only the invalid dependencies are printed.
var msg = new System.IO.StringWriter();
DependencyCheckResultWriter
    .WithResult(checkedDependencies)
    .WithWriter(msg)
    .OnlyInvalidDependencies()
    .Write();

// 5. Use iTernity.plantuml nuget package to attach a PlantUML link to your error message to visualize all components and dependencies.
var plantUmlCode = new System.IO.StringWriter();
PlantUMLWriter
    .WithRule(archStyle)
    .WithWriter(plantUmlCode)
    //.OnlyInvalidDependencies() // Use this, if you only want to display the invalid depedencies.
    .ExcludeLayerDependencies()
    .Write(dependencies);
msgWriter.WriteLine(Iternity.PlantUML.PlantUMLUrl.UML(plantUmlCode.ToString()));

// 6. Assert that all dependencies are allowed
Assert.IsTrue(checkedDependencies.AllDependenciesAllowed, msgWriter.ToString());
```

## Defintion: Components and Dependencies

The archlint project provides a predefined definition for "Components" and "Component Dependencies":

* Every namespace decleration indicates a component.
* Every using statement indicates an outgoing component dependency.

Example: 
```c#
using Petshop.Core;
using System;

namespace PetShop.Order 
{
	...
}
```
In this example archlint would identify two dependencies:
* One dependency from a component called `Petshop.Order` to a component called `Petshop.Core`.
* Another dependency from a component called `Petshop.Order` to a component called `System`.

## Architectural styles

The archlint project provides different predefined architectural styles, which are described below.
An archictural style is a formal description of your architectural conventions (especially dependency rules between components).

If your style is not covered here feel free to define your own or (even better) contribute to the archlint project.
We'd be more than happy if the core model and services becomes even more flexible and extensible.

### Clean Architecture Light (two layers)

This architecture consists of two concentric layers inspired by Bob Martins 
[Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html).

The inner layer is called "CORE". It represents the core domain logic and domain model.

The outer layer is called "IO" (Input/Output). It contains components that connect to an 
external process, technology or user interaction.

Besides the two (IO and CORE) there are two more kinds of components in this architectural style.
* MAIN - A component that assembles all components and bootstraps the app.
* UNKNOWN - Anything that is not of interest. 

The dependency rules of this architectural style are:
* Every component may depend on CORE components.
* An IO component may not depend on other IO components.
* A CORE component may not depend on any IO component.
* Any component other than CORE and IO does not have any restrictions.

To identify the different kinds of components (MAIN, IO, CORE, UNKNOWN) this architectural style uses 
following naming convention with `.` (dot) seperated component names:
* Every component of kind MAIN, IO and CORE has the same, user defined prefix. 
* Every component of kind MAIN has a name with one segment (no dot).
* Every component of kind CORE has a name with two segments (one dot).
* Every component of kind IO has a name with three segments (two dots).
* Every component that does not fit into one of the above definitions is of kind UNKNOWN.

The user defined prefix could be the main domain of the applications for example.

Example: Let's design a Petshop app. For our components we choose the prefix `Petshop`.
* `namespace Petshop` indicates a MAIN component. Petshop is the main domain of this app.
* `namespace Petshop.Order` indicates a CORE component. Order is a subdomain of Petshop.
* `namespace Petshop.Order.Mobile` indicates an IO component. Mobile is a specific but replaceable UI technology to interact with the Order subdomain.
* `namespace Petshop.Order.MySQL` indicates an IO component. MySQL is a specific but replacable persistence technology to store Order data. 
